"""Test the functions to generate devices"""

# 2020-02-07
# Laudin Alessandro Molina Troconis <laudin.molina@imt-atlantique.fr>

import random
from ble_simulator.base import airtime
from ble_simulator.base import _gen_all_devices
from ble_simulator.base import _gen_one_device


random.seed(0)


def test_start_end():
    """End time minus start time should be equal to the airtime"""

    payload = 14
    interval = 20000
    simulation_time = 120000
    rssi = 0
    device = _gen_one_device(0, interval, payload, simulation_time, rssi)

    assert 4 <= len(device) <= 6
    assert device[0].end - device[0].start - airtime(payload) < 0.0001
    assert device[1].end - device[1].start - airtime(payload) < 0.0001
    assert device[2].end - device[2].start - airtime(payload) < 0.0001
    assert device[3].end - device[3].start - airtime(payload) < 0.0001


def test_interval():
    """Time between the frames should be within [interval, interval + 10ms]"""

    payload = 14
    interval = 20000
    simulation_time = 120000
    rssi = 0
    device = _gen_one_device(0, interval, payload, simulation_time, rssi)

    assert 4 <= len(device) <= 6
    assert interval <= device[1].start - device[0].end <= interval + 10000
    assert interval <= device[2].start - device[1].end <= interval + 10000
    assert interval <= device[3].start - device[2].end <= interval + 10000


def test_gen_all_devices():
    payload_size = 31
    interval = 100000
    simulation_time = 300000 + 3 * 10000 + 3 * airtime(payload_size)
    rssi = 0
    conf = {}

    for i in range(3):
        conf[i] = {'interval': interval,
                   'payload_size': payload_size,
                   'simulation_time': simulation_time,
                   'rssi': rssi}

    devices = _gen_all_devices(conf)

    assert len(devices) == 3
    assert 3 <= len(devices[0]) <= 4
    assert 3 <= len(devices[1]) <= 4
    assert 3 <= len(devices[2]) <= 4

    # Generation of addrs is random, thus there is a chance for the address
    # to be the same
    assert devices[0][0].address != devices[1][0].address
    assert devices[0][0].address != devices[2][0].address
    assert devices[1][0].address != devices[2][0].address

# vim:set et sw=4 ts=8 tw=78:
