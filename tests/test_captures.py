"""Test the method that decides if weather a frames captures the channel"""

# 2021-06-10
# Laudin Alessandro Molina Troconis <laudin.molina@imt-atlantique.fr>

from ble_simulator.base import Frame
from ble_simulator.base import airtime


def test_captures_1():
    """Two frames with same RSSI and same start and end time should, none
    captures the channel"""

    payload = 31
    start = 1
    end = start + airtime(payload)
    rssi = 0
    strength_threshold_1 = 999
    strength_threshold_2 = -999
    time_threshold = airtime(payload)

    frame1 = Frame('frame1', start, end, payload, rssi)
    frame2 = Frame('frame2', start, end, payload, rssi)

    assert frame1.captures(frame2, strength_threshold_1, strength_threshold_2,
                           time_threshold) is False
    assert frame2.captures(frame1, strength_threshold_1, strength_threshold_2,
                           time_threshold) is False


def test_captures_2():
    """No capture for two frames with same RSSI and arbitrary overlap"""

    payload = 14
    start_1 = 1
    end_1 = start_1 + airtime(payload)
    start_2 = start_1 + 1
    end_2 = start_2 + airtime(payload)
    rssi = 0
    strength_threshold_1 = 999
    strength_threshold_2 = -999
    time_threshold = airtime(payload)

    frame1 = Frame('frame1', start_1, end_1, payload, rssi)
    frame2 = Frame('frame2', start_2, end_2, payload, rssi)

    assert frame1.captures(frame2, strength_threshold_1, strength_threshold_2,
                           time_threshold) is False
    assert frame2.captures(frame1, strength_threshold_1, strength_threshold_2,
                           time_threshold) is False


def test_captures_3():
    """Much stronger frame captures weaker frame"""
    payload = 31

    rssi_1 = -20
    start_1 = 1
    end_1 = start_1 + airtime(payload)

    start_2 = start_1
    end_2 = start_2 + airtime(payload)
    rssi_2 = -80

    strength_threshold_1 = 10
    strength_threshold_2 = 999
    time_threshold = 0

    frame1 = Frame('frame1', start_1, end_1, payload, rssi_1)
    frame2 = Frame('frame2', start_2, end_2, payload, rssi_2)

    assert frame1.captures(frame2, strength_threshold_1, strength_threshold_2,
                           time_threshold) is True
    assert frame2.captures(frame1, strength_threshold_1, strength_threshold_2,
                           time_threshold) is False


def test_captures_4():
    """Stronger first captures the channel"""

    payload = 31

    rssi_1 = -20
    start_1 = 0
    end_1 = start_1 + airtime(payload)

    start_2 = start_1 + 1
    end_2 = start_2 + airtime(payload)
    rssi_2 = -24

    strength_threshold_1 = 8
    strength_threshold_2 = 4
    time_threshold = 1

    frame1 = Frame('frame1', start_1, end_1, payload, rssi_1)
    frame2 = Frame('frame2', start_2, end_2, payload, rssi_2)

    assert frame1.captures(frame2, strength_threshold_1, strength_threshold_2,
                           time_threshold) is True
    assert frame2.captures(frame1, strength_threshold_1, strength_threshold_2,
                           time_threshold) is False


def test_captures_5():
    """Emulate the case were the frame that arrives first captures the
    channel"""

    payload = 31
    rssi = 0

    start_1 = 0
    end_1 = start_1 + airtime(payload)

    start_2 = start_1 + 1
    end_2 = start_2 + airtime(payload)

    frame1 = Frame('frame1', start_1, end_1, payload, rssi)
    frame2 = Frame('frame2', start_2, end_2, payload, rssi)

    strength_threshold_1 = 999
    strength_threshold_2 = -999
    time_threshold = 1

    assert frame1.captures(frame2, strength_threshold_1, strength_threshold_2,
                           time_threshold) is True
    assert frame2.captures(frame1, strength_threshold_1, strength_threshold_2,
                           time_threshold) is False


def test_captures_6():
    """Check same RSSI, delta is below threshold. None of the frames captures
    the channel"""

    strength_threshold_1 = 8
    strength_threshold_2 = 0
    time_threshold = 8

    payload = 31
    rssi = 0

    start_1 = 0
    end_1 = start_1 + airtime(payload)

    start_2 = start_1 + time_threshold - 1
    end_2 = start_2 + airtime(payload)

    frame1 = Frame('frame1', start_1, end_1, payload, rssi)
    frame2 = Frame('frame2', start_2, end_2, payload, rssi)

    assert frame1.captures(frame2, strength_threshold_1, strength_threshold_2,
                           time_threshold) is False
    assert frame2.captures(frame1, strength_threshold_1, strength_threshold_2,
                           time_threshold) is False


def test_captures_7():
    """Check same RSSI, delta is above threshold. First of the frames captures
    the channel"""

    strength_threshold_1 = 999
    strength_threshold_2 = -999
    time_threshold = 8

    payload = 31
    rssi = 0

    start_1 = 0
    end_1 = start_1 + airtime(payload)

    start_2 = start_1 + time_threshold + 1
    end_2 = start_2 + airtime(payload)

    frame1 = Frame('frame1', start_1, end_1, payload, rssi)
    frame2 = Frame('frame2', start_2, end_2, payload, rssi)

    assert frame1.captures(frame2, strength_threshold_1, strength_threshold_2,
                           time_threshold) is True
    assert frame2.captures(frame1, strength_threshold_1, strength_threshold_2,
                           time_threshold) is False

def test_captures_8():
    rssi = 0
    payload = 31

    strength_threshold_1 = 999
    strength_threshold_2 = -999
    time_threshold = 8

    start_1 = 24757
    end_1 = 251330

    start_2 = 24557
    end_2 = 24933

    frame1 = Frame('frame1', start_1, end_1, payload, rssi)
    frame2 = Frame('frame2', start_2, end_2, payload, rssi)

    assert frame1.captures(frame2, strength_threshold_1, strength_threshold_2,
                           time_threshold) is False
    assert frame2.captures(frame1, strength_threshold_1, strength_threshold_2,
                           time_threshold) is True


def test_overlap_1():
    """Check for non overlapping frames"""

    payload = 31
    rssi = 0

    start_1 = 0
    end_1 = start_1 + airtime(payload)

    start_2 = end_1 + 1
    end_2 = start_2 + airtime(payload)

    frame1 = Frame('frame1', start_1, end_1, payload, rssi)
    frame2 = Frame('frame2', start_2, end_2, payload, rssi)

    assert frame1.overlaps(frame2) is False
    assert frame2.overlaps(frame1) is False


def test_overlap_2():
    """Check for two overlapping frames"""

    payload = 31
    rssi = 0

    start_1 = 0
    end_1 = start_1 + airtime(payload)

    start_2 = 0
    end_2 = start_2 + airtime(payload)

    frame1 = Frame('frame1', start_1, end_1, payload, rssi)
    frame2 = Frame('frame2', start_2, end_2, payload, rssi)

    assert frame1.overlaps(frame2) is True
    assert frame2.overlaps(frame1) is True


# vim:set et sw=4 ts=8 tw=78:
