"""Test the functions and classes from sim_advertisements"""

# 2020-02-07
# Laudin Alessandro Molina Troconis <laudin.molina@imt-atlantique.fr>


from ble_simulator.base import Frame
from ble_simulator.base import check_collisions
from ble_simulator.base import adv_delay_625

def test_frame_defaults():
    """Using no parameters should invoke defaults."""
    f1 = Frame()
    f2 = Frame(None, None, None)
    assert (f1.address, f1.start, f1.end, f1.collision) == \
            (f2.address, f2.start, f2.end, f2.collision)


def test_frame_collide_1():
    """Collision test should return the same for both frames"""

    f1 = Frame(0, 0, 10)
    f2 = Frame(1, 5, 15)

    assert f1.collide(f2) == f2.collide(f1) == True

def test_frame_collide_2():
    """Check the method that tests collisions between two frames"""

    f1 = Frame(0, 0, 10)
    f2 = Frame(1, 5, 15)

    assert f1.collide(f2) == True

def test_frame_collide_3():
    """Check the method that tests collisions between two frames"""

    f1 = Frame(0, 0, 10)
    f2 = Frame(1, 50, 60)

    assert f1.collide(f2) == False

def test_frame_collide_4():
    """Check the method that tests collisions between two frames"""

    f1 = Frame(1, 5, 15)
    f2 = Frame(0, 0, 10)

    assert f1.collide(f2) == True


def test_frame_collide_5():
    """Check the method that tests collisions between two frames"""

    f1 = Frame(1, 0, 10)
    f2 = Frame(0, 0, 10)

    assert f1.collide(f2) == True

def test_frame_collide_6():
    """Check the method that tests collisions between two frames"""

    f1 = Frame(1, 0, 10)
    f2 = Frame(0, 10, 20)

    assert f1.collide(f2) == True


def test_frame_collide_7():
    """Check the method that tests collisions between two frames"""

    f1 = Frame(1, 10, 20)
    f2 = Frame(0, 0, 10)

    assert f1.collide(f2) == True


def test_check_collisions_1():
    devices = [
        [Frame(0, 0, 10), Frame(0, 20, 30), Frame(0, 40, 50)],
    ]

    f = Frame(1, 0, 10)

    assert check_collisions(f, devices) == 1
    assert f.collision == True
    assert devices[0][0].collision == True
    assert devices[0][1].collision == False
    assert devices[0][2].collision == False


def test_check_collisions_2():
    devices = [
        [Frame(0, 0, 10), Frame(0, 20, 30), Frame(0, 40, 50)],
        [Frame(1, 15, 19), Frame(1, 35, 39), Frame(1, 55, 59)],
    ]

    f = Frame(1, 10, 15)

    assert check_collisions(f, devices) == 2
    assert f.collision == True
    assert devices[0][0].collision == True
    assert devices[0][1].collision == False
    assert devices[0][2].collision == False

    assert devices[1][0].collision == True
    assert devices[1][1].collision == False
    assert devices[1][2].collision == False


def test_check_collisions_3():
    devices = [
        [Frame(0, 0, 10), Frame(0, 20, 30), Frame(0, 40, 50)],
        [Frame(1, 15, 19), Frame(1, 35, 39), Frame(1, 55, 59)],
    ]

    f = Frame(1, 100, 115)

    assert check_collisions(f, devices) == 0
    assert f.collision == False
    assert devices[0][0].collision == False
    assert devices[0][1].collision == False
    assert devices[0][2].collision == False

    assert devices[1][0].collision == False
    assert devices[1][1].collision == False
    assert devices[1][2].collision == False


def test_adv_delay_625():
    for _ in range(100):
        adv_delay = adv_delay_625()
        assert 0 <= adv_delay <= 10000
        rest = adv_delay % 625
        assert rest == 0

# vim:set et sw=4 ts=8 tw=78:
