BLE Advertisements Collision Simulator
======================================

This code allows the simulation of BLE collisions.

In BLE, advertisers broadcast `advertisements` in three advertisement
channels. The advertisements occurs at `advertisement events`. Advertisers
transmit `adervertisements` every ``advInterval + advDelay``, where
`advInterval` is a parameter and `advDelay` is a random value in the range
[0ms, 10ms].

When multiple advertisers operate in the vicinity, the advertisements
eventually collide. Advertisements collision may result in the inability of a
receiver to decode the advertisements.

The simulator considers that the scanner can decode all the
advertisements that it receives (i.e., signal strength is above the
sensibility level) unless there is a collision.

The simulations model ideal scenarios, without external interference or
variations on the radio. There are two posibilities: to model *Total
Collision*. That is, the receiver misses any frames that overlaps with other
frame.  This simulation results in the worst possible PDR for a given
scenario, i.e., a lower bound. A second simulation models *Capture Effect*.
This second simulation takes an optimistic approach and assumes that, in case
of overlapping frames, the receiver decodes the frame that arrives first. The
simulation *Capture Effect* provides the best possible PDR for a given
scenario, i.e., an upper bound.


Overview of the Algorithm
=========================

For each of the advertisers, the simulator generates an array of
advertisements, each advertisement includes time stamps for the beginning and
end of the transmission. The interval between the advertisements of the same
advertise (*T_advEvent*) has two components: *advInterval*, which is constant,
and *advDelay*, which is variable.  The simulator randomly selects *advDelay*
from the range [0ms, 10ms], with steps of 0.625ms. The simulator then checks
for collisions by comparing the transmission time stamps.
