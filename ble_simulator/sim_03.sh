#!/usr/bin/env bash
#
# Simulate advertisements to compare with preliminary experiments after
# Elisa's findings about the adv_delay in TI and Nordic cards.

SEED=0
INTERVALS=(20 30 50 100 250)
PAYLOAD=14
DEVICES=6
TIME=300000 # 5 min
#TIME=10000 # 10 sec

outfile="sim_03.csv"
echo "${0} git rev $(git rev-parse HEAD) ; $(date) ; $(hostname -f)" | \
  tee ${outfile}

python3 sim_advertisements.py \
  --payload ${PAYLOAD} \
  --devices ${DEVICES} \
  --seed ${SEED} \
  --interval ${INTERVALS[0]} \
  --time ${TIME} | \
    tee -a ${outfile}

for interval in ${INTERVALS[@]:1};
do
  python3 sim_advertisements.py \
    --payload ${PAYLOAD} \
    --devices ${DEVICES} \
    --seed ${SEED} \
    --interval ${interval} \
    --time ${TIME} \
    --no-header | \
    tee -a ${outfile}
done
