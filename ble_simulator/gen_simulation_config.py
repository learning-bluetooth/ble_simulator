#!/usr/bin/env python3

import argparse
from pathlib import Path
import pandas as pd


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", required=True, help="Path to the traces")
    args = parser.parse_args()
    file_path = args.path

    df = pd.read_csv(file_path, skiprows=1, header=None)
    df.columns = ('address', 'rssi_mean', 'interval', 'payload_size', 'data')

    out_name = '_'.join(Path(file_path).parts[1:]) + '.ini'
    outfile = open(out_name, 'w')
    print('[main]', file=outfile)
    print('seed = 0', file=outfile)
    print('simulation_time = 10000', file=outfile)
    print('num_trials = 30', file=outfile)
    print(file=outfile)

    for dev in df.iterrows():
        interval = float(dev[1]['interval'].split(', ')[1][:-1]) - 5
        # interval = int(dev[1]['interval'].split(', ')[0][1:])
        nobs = len(dev[1]['data'].split())
        print('[dev{}]'.format(dev[0]), file=outfile)
        print('nobs = ', nobs, file=outfile)
        print('address = ', dev[1]['address'], file=outfile)
        print('payload_size = ', dev[1]['payload_size'], file=outfile)
        print('interval = ', interval, file=outfile)
        print('rssi_mean = ', dev[1]['rssi_mean'], file=outfile)
        print(file=outfile)

    outfile.close()


if __name__ == "__main__":
    main()
