#!/usr/bin python3

"""
Simulate BLE advertisements to evaluate the capacity, i.e., the number of
collisions

2020-02-06
Laudin Alessandro Molina Troconis <laudin.molina@imt-atlantique.fr>
"""

import os
import os.path
import logging
import random
import argparse

import numpy as np
import statsmodels.stats.api as sms

from base import prepare_devices_with_same_conf
from base import simulate
from base import generate_advertisements


SCRIPT_NAME = os.path.splitext(os.path.basename(__file__))[0]

logging.basicConfig()
log = logging.getLogger(SCRIPT_NAME)  # pylint: disable=invalid-name
log.setLevel(logging.DEBUG)


def main():  # pylint: disable=missing-function-docstring
    parser = argparse.ArgumentParser()
    parser.add_argument("--num_trials", default=30, type=int,
                        help="Number of trials to run (30 by default)")
    parser.add_argument("--payload", default=31, type=int,
                        help="Payload size in bytes (31 by default)")
    parser.add_argument("--devices", default=6, type=int,
                        help="Number of devices to simulate (6 by default)")
    parser.add_argument("--time", default=10000000, type=int,
                        help="Simulation time in ms (10000000 us by default)")
    parser.add_argument("--seed", default=-1, type=int,
                        help="Seed for the random generator")
    parser.add_argument("--interval", default=100000, type=int,
                        help="Interval to simulate (100000 us by default)")
    parser.add_argument("--name", help="A name for the simulation")
    parser.add_argument("--header", default=True, action='store_true',
                        help="Show a column header or not")
    parser.add_argument("--no-header", dest='header', action='store_false')
    parser.add_argument("--stats-footer", default=False,
                        dest='stats_footer', action='store_true',
                        help="Show a sum up line at the end with the "
                        "average error rate over all trials")
    parser.add_argument("--capture-effect", default=False,
                        dest='capture_effect', action='store_true',
                        help="When present, the simulator will assume that "
                        "one of the frames captures the channel and is "
                        "correctly decoded.")
    parser.add_argument("--no-capture-effect", action='store_false',
                        dest='capture_effect')

    args = parser.parse_args()

    if args.seed >= 0:
        random.seed(args.seed)
    else:
        random.seed(0)

    num_trials = args.num_trials
    payload_size = args.payload
    num_devices = args.devices
    simulation_time = args.time
    interval = args.interval
    name = args.name
    avg_err = 0.0

    adv_params = {
        'interval': interval,
        'payload_size': payload_size,
        'simulation_time': simulation_time,
    }
    devs_conf = prepare_devices_with_same_conf(num_devices, adv_params)

    if args.header:
        print('advInterval,devices,payload,rxError,name')

    results = []
    for _ in range(num_trials):
        # One trial

        # Generate advertisements for all devices
        advertisements = generate_advertisements(devs_conf)

        # Count frames that arrive to the destination (i.e., those that didn't
        # collide)
        result = simulate(advertisements, args.capture_effect)
        results.append(result)
        print('{},{},{},{},{}'.format(
            interval,
            num_devices,
            payload_size,
            result,
            name,
        ))
        avg_err += result

    if args.stats_footer:
        print('mean: ', np.mean(results),
              '\nstd: ', np.std(results),
              '\nmedian: ', np.median(results),
              '\nmin: ', np.min(results),
              '\nmax: ', np.max(results),
              '\nci 95%: ', sms.DescrStatsW(results).tconfint_mean())


if __name__ == "__main__":
    main()

# vim:set et sw=4 ts=8 tw=78:
