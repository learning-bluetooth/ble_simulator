#!/usr/bin/env python3
"""
Simulate BLE advertisements to evaluate the BLE capacity, i.e., the number of
collisions, the fraction of frames received. THis script generates
advertisement event using three aproaches for advDelay: 0.625ms step, 1ms
step, continue step.

2020-04-15
Laudin Alessandro Molina Troconis <laudin.molina@imt-atlantique.fr>
"""


import os
import os.path
import logging
import random
import argparse

from base import _gen_one_device
from base import prepare_devices_with_same_conf
from base import simulate

SCRIPT_NAME = os.path.splitext(os.path.basename(__file__))[0]

logging.basicConfig()
log = logging.getLogger(SCRIPT_NAME)  # pylint: disable=invalid-name
log.setLevel(logging.DEBUG)


def generate_advertisements(conf, rssi_thresh=-999, **kwargs):
    """
    Generate the advertisements for all devices described in the configuration

    :conf: dictionary containing the configuration for each device
    :returns: an array of array containing the advertisement frames for all
    devices
    """

    advertisements = []
    for addr_i, conf_i in conf.items():
        # Only generate advertisements for devices with an RSSI abobe a given
        # threshold
        if 'rssi_mean' in conf_i:
            if conf_i['rssi_mean'] >= rssi_thresh:
                advertisements.append(
                    _gen_one_device(
                        addr_i,
                        conf_i['interval'],
                        conf_i['payload_size'],
                        conf_i['simulation_time'],
                        **kwargs
                    ),
                )
        else:
            advertisements.append(
                _gen_one_device(
                    addr_i,
                    conf_i['interval'],
                    conf_i['payload_size'],
                    conf_i['simulation_time'],
                    **kwargs
                ),
            )

    return advertisements


def main():  # pylint: disable=missing-function-docstring
    parser = argparse.ArgumentParser()
    parser.add_argument("--num_trials", default=30, type=int,
                        help="Number of trials to run (30 by default)")
    parser.add_argument("--payload_size", default=27, type=int,
                        help="Payload size in bytes (27 by default)")
    parser.add_argument("--devices", default=5, type=int,
                        help="Number of devices to simulate")
    parser.add_argument("--time", default=10000000, type=int,
                        help="Simulation time in ms (10000000 us by default)")
    parser.add_argument("--seed", default=-1, type=int,
                        help="Seed for the random generator")
    parser.add_argument("--interval", default=100000, type=int,
                        help="Interval to simulate (100000 us by default)")
    parser.add_argument("--header", default=True, action='store_true',
                        help="Show a column header or not")
    parser.add_argument("--no-header", dest='header', action='store_false')
    args = parser.parse_args()

    if args.seed >= 0:
        random.seed(args.seed)

    adv_params = {}
    adv_params['interval'] = args.interval
    adv_params['payload_size'] = args.payload_size
    adv_params['simulation_time'] = args.time
    devs_conf = prepare_devices_with_same_conf(args.devices,
                                               adv_params)

    adv_delay_algs = {
        '625': lambda: random.randrange(0, 10000 + 1, 625),
        '1000': lambda: random.randint(0, 10) * 1000,
        'float': lambda: random.uniform(0, 10000),
    }

    if args.header:
        print('advInterval,devices,payload,rxError,advDelay')

    results = []
    for adv_delay_name, adv_delay in adv_delay_algs.items():
        for _ in range(args.num_trials):
            # One trial

            # Generate advertisements for all devices according to the
            # configuration
            advertisements = generate_advertisements(devs_conf,
                                                     adv_delay=adv_delay)

            # Count frames that arrive to the destination (i.e., those that
            # didn't collide)
            result = simulate(advertisements)
            results.append(result)
            print('{},{},{},{},{}'.format(
                args.interval,
                args.devices,
                args.payload_size,
                result,
                adv_delay_name
            ))


if __name__ == "__main__":
    main()

# vim:set et sw=4 ts=8 tw=78:
