#!/usr/bin/env bash
# Check the rxError for different algorithms to generate the advDelay

outfile="sim_02.csv"
echo "${0} git rev $(git rev-parse HEAD) ; $(date) ; $(hostname -f)" | \
  tee ${outfile}

#size=31
#interval=${1}
#for devices in $(seq 1 9) $(seq 10 10 100);
##for devices in $(seq 1 3);
#do
	#python3 compare_advalgs.py \
		#--num_trials 30 \
		#--payload_size $size \
		#--time 10000 \
		#--seed 1 \
		#--devices $devices \
		#--interval $interval \
		#--no-header
#done

(echo 20 ; seq 50 50 1000) | \
  parallel -k -I @@ \
  '(seq 1 9 ; seq 10 10 100) | parallel -k \
    python3 compare_advalgs.py \
      --num_trials 30 \
      --payload_size 31 \
      --time 10000 \
      --seed 1 \
      --devices {} \
      --interval @@ \
      --no-header' | \
      tee -a ${outfile}
