#!/usr/bin/env bash
#
# Simulate advertisements to compare the effects different frame sizes on the
# capture effect
#
# For reference, experimental traces below also compare the frame size and the
# PDR
# <PARTAGE>/Chantier 3.0/traces/faraday/38/payload16/100ms/
# <PARTAGE>/Chantier 3.0/traces/faraday/38/payload24/100ms/
# <PARTAGE>/Chantier 3.0/traces/faraday/38/payload31/100ms/
#
# This simulation uses parameters that are similar to the sim_05 with the
# difference that here we introduce capture effect, we allow the scanner to
# always decode one of the advertisements in a collison
#
# --capture-effect advDelay = 100ms payload = 16 devices = 1 to 1000
# --capture-effect advDelay = 100ms payload = 24 devices = 1 to 1000
# --capture-effect advDelay = 100ms payload = 31 devices = 1 to 1000
# --no-capture-effect advDelay = 100ms payload = 16 devices = 1 to 1000
# --no-capture-effect advDelay = 100ms payload = 24 devices = 1 to 1000
# --no-capture-effect advDelay = 100ms payload = 31 devices = 1 to 1000

tmp="$(basename $0)"
outfile="${tmp%.*}.csv"

SEED=0
PAYLOAD=($(seq 2 2 32))
INTERVALS=(100)
DEVICES=($(seq 1 1 15) $(seq 20 10 100) $(seq 100 100 1000))
TIME=2000 # 2 seconds
TRIALS=30

echo "${0} git rev $(git rev-parse HEAD) ; $(date) ; $(hostname -f)" | \
  tee ${outfile}

# Quick hack to get the CSV header
python3 sim_advertisements_devices.py \
  --num_trials ${TRIALS} \
  --capture-effect \
  --payload ${PAYLOAD[0]} \
  --devices ${DEVICES[0]} \
  --seed ${SEED} \
  --interval ${INTERVALS[0]} \
  --time ${TIME} \
  --name "hack" | \
  tee -a ${outfile}

for payload in ${PAYLOAD[@]}; do
  printf '%s\n' "${DEVICES[@]}" | parallel -k \
    python3 sim_advertisements_devices.py \
    --num_trials ${TRIALS} \
    --capture-effect \
    --payload ${payload} \
    --devices {} \
    --seed ${SEED} \
    --interval ${INTERVALS[0]} \
    --time ${TIME} \
    --no-header \
    --name "capture" | \
    tee -a ${outfile}

  printf '%s\n' "${DEVICES[@]}" | parallel -k \
    python3 sim_advertisements_devices.py \
    --num_trials ${TRIALS} \
    --no-capture-effect \
    --payload ${payload} \
    --devices {} \
    --seed ${SEED} \
    --interval ${INTERVALS[0]} \
    --time ${TIME} \
    --no-header \
    --name "no-capture" | \
    tee -a ${outfile}
done
