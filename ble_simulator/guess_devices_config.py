#!/usr/bin/env python

import pandas as pd
from scipy import stats
import numpy as np

def describe(x):
    if len(x) < 2:
        return pd.Series(name='metrics')

    intervals = x['time'].iloc[1:].values - x['time'].iloc[:-1].values
    tmp = stats.describe(intervals)
    result = {'nobs': tmp.nobs,
            'min': tmp.minmax[0],
            'max': tmp.minmax[1],
            'mean': tmp.mean,
            'variance': tmp.variance,
            'median': np.median(intervals)
           }
    return pd.Series(result, name='metrics')

def guess_config(x):
    global count

    if len(x) < 2:
        return pd.Series(name='config')

    intervals = x['time'].iloc[1:].values - x['time'].iloc[:-1].values
    result = {'nobs': intervals.size,
              'payload_size': x.iloc[0]['payload_size'],
              'interval': np.median(intervals) - 5,
             }
    count += 1
    if result['nobs'] > 0:
        print('[dev%s]' % count)
        print('address = %s' % x.iloc[0]['address'])
        print('nobs = %s' % result['nobs'])
        print('payload_size = %s' % int(result['payload_size']))
        print('interval = %s' % int(result['interval']))
        print()
    return pd.Series(result, name='config')

def filter_out_other_channels(df, address, interval, lost_threshold=100):
    """Use the address and the time to filter frames from other channels

    If our device is not heard in a given interval remove the devices heard in
    that interval.
    """
    mydev = df.query('address == @address')

    first = mydev.iloc[0]['time']
    intervals = []
    for i in range(1, len(mydev)):
        # Threshold indicates the number of frames that allow to loss.
        if mydev.iloc[i]['time'] - mydev.iloc[i - 1]['time'] > \
           lost_threshold * interval:

            last = mydev.iloc[i - 1]['time']
            intervals.append((first, last))
            first = mydev.iloc[i]['time']
    intervals.append((first, mydev.iloc[-1]['time']))
    first, last = intervals.pop()

    tmp = [df.query('time >= @first & time <= @last')]
    for first, last in intervals:
        aux = df.query('time >= @first & time <= @last')
        tmp.append(df.query('time >= @first & time <= @last'))

    df_filtered = pd.concat(tmp)

    return df_filtered

def main(file_path, address=None):
    df = pd.read_csv(file_path,
                     delimiter=':',
                     names=('address', 'rssi', 'time', 'payload_size'))

    # The interval should be the median minus the expected advDelay, where
    # advDelay is a random between 0 and then. Thus the expected advDelay
    # should be (10 - 0) / 2 => 5
    if address is not None:
        our_interval = describe(df.query('address == @address'))['median'] - 5
        print(f'# Guessed interval for {address} is {our_interval}ms')

        df = filter_out_other_channels(df, address, our_interval)

    global count
    count = 0
    df.groupby('address').apply(guess_config)
    print(df.groupby('address').apply(describe))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--path", help="Path to the traces", required=True)
    parser.add_argument("--address", default=None,
                        help="Address of our device")
    args = parser.parse_args()
    file_path = args.path
    address = args.address
    main(file_path, address)

