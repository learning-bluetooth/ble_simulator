#!/usr/bin/env bash
#
# Simulate advertisements to compare with empirical results, compare with
# experimental
# <PARTAGE>/Chantier 3.0/traces/faraday/38/payload31/100ms/
#
# This simulation uses parameters that are similar to the sim_05 with the
# difference that here we introduce capture effect, we allow the scanner to
# always decode one of the advertisements in a collison
#
# --capture-effect
# advDelay = 100ms
# payload = 31
# devices = 1 to 15

tmp="$(basename $0)"
outfile="${tmp%.*}.csv"

SEED=0
INTERVALS=(100)
PAYLOAD=31
DEVICES=($(seq 1 1 15) $(seq 20 10 100) $(seq 100 100 1000))
TIME=2000 # 2 seconds
TRIALS=30

echo "${0} git rev $(git rev-parse HEAD) ; $(date) ; $(hostname -f)" | \
  tee ${outfile}

python3 sim_advertisements_devices.py \
	--num_trials ${TRIALS} \
	--capture-effect \
	--payload ${PAYLOAD} \
	--devices ${DEVICES[0]} \
	--seed ${SEED} \
	--interval ${INTERVALS[0]} \
	--time ${TIME} | \
    tee -a ${outfile}

printf '%s\n' "${DEVICES[@]}" | parallel -k \
					 python3 sim_advertisements_devices.py \
					 --num_trials ${TRIALS} \
					 --capture-effect \
					 --payload ${PAYLOAD} \
					 --devices {} \
					 --seed ${SEED} \
					 --interval ${INTERVALS[0]} \
					 --time ${TIME} \
					 --no-header | \
    tee -a ${outfile}
