#!/usr/bin/env bash

ROOT="$HOME/Nextcloud/Chantier 3.0/traces/Tests_collision_capture/tests"
PAYLOADS=(10 15 20 25 31)
#PAYLOADS=(31)
DEVICES=$(seq 2 24)
#DEVICES=$(seq 24 24)
TRIALS=$(seq 1 10)
#TRIALS=$(seq 10 10)

devices=24
trial=1

for payload in ${PAYLOADS[@]}; do
  for devices in ${DEVICES[@]}; do
    for trial in ${TRIALS[@]}; do
      path="${ROOT}/payload${payload}/${devices}cartes/test${trial}/data_TI.csv"
      outfile="payload_${payload}_devices_${devices}_${trial}.ini"
      #echo "$path"
      if test -r "${path}"; then
        python3 gen_simulation_config_from_data_TI.py --path "${path}" > "${outfile}"
      elif test -r "${path}.gz"; then
        python3 gen_simulation_config_from_data_TI.py --path "${path}.gz" > "${outfile}"
      else
        echo "File not found: ${path}"
      fi
    done
  done
done
