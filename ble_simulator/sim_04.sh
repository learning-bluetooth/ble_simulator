#!/usr/bin/env bash
#
# Simulate advertisements to compare with empirical results, compare with
# experimental
# <PARTAGE>/Chantier 3.0/traces/faraday/3canaux/payload31/100ms/test<trial 2>/<num cartes>/test.csv
#
# advDelay = 100ms
# payload = 31
# devices = 1 to 15

SEED=0
INTERVALS=(100)
PAYLOAD=31
DEVICES=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15)
TIME=300000 # 5 min

outfile="sim_04.csv"
echo "${0} git rev $(git rev-parse HEAD) ; $(date) ; $(hostname -f)" | \
  tee ${outfile}

python3 sim_advertisements.py \
  --payload ${PAYLOAD} \
  --devices ${DEVICES[0]} \
  --seed ${SEED} \
  --interval ${INTERVALS[0]} \
  --time ${TIME} | \
    tee -a ${outfile}

for devices in ${DEVICES[@]:1};
do
  python3 sim_advertisements.py \
    --payload ${PAYLOAD} \
    --devices ${devices} \
    --seed ${SEED} \
    --interval ${INTERVALS[0]} \
    --time ${TIME} \
    --no-header | \
    tee -a ${outfile}
done
