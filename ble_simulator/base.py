#!/usr/bin/env python3
#
# Simulate BLE advertisements to evaluate the capacity, i.e., the number of
# collisions
#
# 2020-02-06
# Laudin Alessandro Molina Troconis <laudin.molina@imt-atlantique.fr>

import random
import logging
import configparser
from bisect import bisect_left


# Check Spec 5.2, Vol 1, Part A, Sec 3.2.2, Table 3.2: On-air time for various
# advertising events (page 222).  According to Table 3.2, an advertising event
# with AdvData of 31 Bytes takes 1128 microseconds. Check the note below Table
# 3.2, the note says that the author used three PDUs to compute the numbers.
# Is this because advertisements happens on three channels, thus there will be
# three PDUs? If that is the case, then the function is OK, otherwise, the
# function is returning 1/3 of the expected airtime
#
# AdvData Airtime
# 0         384
# 15        744
# 31        1128
#
# That is for connectable undirected advertisement events
def airtime(adv_data_size, data_rate=10**6):
    """Estimates the time required to transmit a BLE advertisement PDU

    :adv_data_size: bytes used in the advertisent payload
    :data_rate: transmission data rate in bps. By default is 1 Mbps
    :returns: airtime in milliseconds
    """

    # The size of the advertisement packet on BLE 4.2 (legacy advertisement)
    # (https://www.rfwireless-world.com/Terminology/BLE-Advertising-and-Data-Packet-Format.html):
    # References:
    # Spec 5.2, Vol 6, Part B, Sec 2.1,
    # Spec 5.2, Vol 6, Part B, Sec 2.3,
    # Spec 5.2, Vol 6, Part B, Sec 2.3, Fig 2.4
    # Spec 5.2, Vol 6, Part B, Sec 2.3.1.3, Fig 2.4, Fig 2.5, and Fig 2.8:
    # ADV_NONCONN_IND PDU Payload (p. 2874)

    # For connectable, undirected advertisements:
    # Preamble                   1 Byte
    # Access Address             4 Bytes
    #   Advertisement PDU
    #     Advertisement Header   2 bytes
    #     Payload
    #       AdvAddress           6 Bytes
    #       AdvData         0 - 31 Bytes -> 14 Bytes on Elisa's configuration
    # CRC                        3 Bytes
    #                   ----------------
    # Total                16 - 47 Bytes

    # convert to bits
    adv_size = (1 + 4 + 2 + 6 + adv_data_size + 3) * 8

    # airtime in seconds
    adv_airtime = adv_size / data_rate

    # convert to us
    adv_airtime *= 1000000

    return adv_airtime


def adv_delay_625():
    """
    Generate a random delay between 0 and 10000 us (10 ms), in steps of 625 us.
    """

    return random.randrange(0, 10000 + 1, 625)


def gen_random_address():
    """Generate a random address"""
    return "%02x:%02x:%02x:%02x:%02x:%02x" % (random.randint(0, 255),
                                              random.randint(0, 255),
                                              random.randint(0, 255),
                                              random.randint(0, 255),
                                              random.randint(0, 255),
                                              random.randint(0, 255))


class Frame:
    def __init__(self, address=None, start=None, end=None, payload_size=None,
                 rssi=None, interval=100):
        self.address = address
        self.start = start
        self.end = end
        self.payload_size = payload_size
        self.collision = False
        self.rssi = rssi
        self.interval = interval

    def overlaps(self, other):
        retval = True
        if self.end < other.start:
            retval = False
        elif self.start > other.end:
            retval = False

        return retval

    def collide(self, other: 'Frame') -> bool:
        logging.warning('Deprecated. '
                        'You should use the method "overlaps" instead')
        return self.overlaps(other)

    def captures(self, other: 'Frame') -> bool:
        """Checks if current frames captures the channel over another frame
        """

        # TODO: should we check for overlapping before checking for capture or
        # just assume that we only call the function when frames overlaps

        delta_rssi = self.rssi - other.rssi
        clear_transmission = (other.start - self.start) / \
                airtime(self.payload_size)

        # capture = False
        # if delta_rssi <= 10:
        #    capture = False
        # elif delta_rssi <= 2:
            # if clear_transmission > 0.87:
                # capture = True
        # elif delta_rssi <= 3:
        #     if clear_transmission > 0.73:
        #         capture = True
        # elif delta_rssi <= 4:
        #     if clear_transmission > 0.37:
        #         capture = True
        # else:
            # capture = True

        capture = False

        if delta_rssi >= 8:
            capture = True
        if delta_rssi >= 7 and clear_transmission > 0.2:
            capture = True
        elif delta_rssi >= 6 and clear_transmission > 0.4:
            capture = True
        # elif delta_rssi >= 4 and clear_transmission > 0.9:
            # capture = True
        else:
            capture = False

        return capture

    def __repr__(self):
        return f'addr: {self.address} / start: {self.start} / end: {self.end}'

    def __lt__(self, other):
        return self.end < other.start


# def _find_rightmost_lte(a, x, i=0, j=None):
    # if j == None:
        # j = len(x)

    # mid = (j - i) // 2

    # if a < x[mid]:
        # return _find_rightmost_lte(a, x, i, mid)
    # else:
        # return mid


def check_collisions(frame, devices, capture_effect=False,
                     power_threshold_1=999, power_threshold_2=-999,
                     time_threshold=8):
    """Check and mark frames that overlaps.

    If capture_effect=True, then it assumes that one of the frames is always
    decoded.
    """

    logging.debug('capture_effect is set to %s', capture_effect)
    collision_count = 0
    for device in devices:
        # for other in device:
        start = bisect_left(device, frame)
        for i in range(start, len(device)):
            other = device[i]

            if other.start > frame.end:
                break

            if other is not frame and frame.overlaps(other):
                collision_count += 1

                if capture_effect:
                    if frame.captures(other):
                        other.collision = True
                    else:
                        frame.collision = True

                    if other.captures(frame):
                        frame.collision = True
                    else:
                        other.collision = True

                else:
                    frame.collision = True
                    other.collision = True

                if frame.collision is False and other.collision is False:
                    print('This should never happen ', frame, other)

    return collision_count


def _gen_one_device(address, interval, payload_size, simulation_time, rssi,
                    **kwargs):

    # According to Bluetooth Spec v5.1 Section 4.4.2.2.1 (Fig 4.3), the
    # time between two consecutive advertising events is computed as follows:
    #
    # T_advEvent = advInterval + advDelay
    #
    # where
    # T_advEvent is the time between two consecutive events
    # advInterval is the advertising interval, an integer multiple of 0.625ms
    # in the range 20ms to 10.485759375ms
    # advDelay is a pseudo-random value in the range 0ms to 10ms

    if 'adv_delay' in kwargs:
        adv_delay = kwargs.get('adv_delay')
        logging.info('Using the specified advDelay')
    else:
        adv_delay = adv_delay_625
        logging.info('Using the default advDelay')

    logging.debug('Generating advertisements for device. Addr: %s '
                  'advInterval: %s Payload size: %s Simulation time: %s',
                  address, interval, payload_size, simulation_time)

    boot_time = random.randint(0, interval)
    frame_airtime = airtime(payload_size)

    logging.debug('boot_time %d, frame_airtime %d', boot_time, frame_airtime)

    device = []
    frame_start = boot_time
    frame_end = frame_start + frame_airtime
    while frame_end < simulation_time:
        device.append(Frame(address, frame_start, frame_end, rssi=rssi,
                            payload_size=payload_size, interval=interval))
        T_advEvent = interval + adv_delay()
        frame_start = frame_start + T_advEvent
        frame_end = frame_start + frame_airtime

    return device


def _gen_all_devices(conf, **kwargs):
    devices = []
    for addr_i, conf_i in conf.items():
        devices.append(
            _gen_one_device(
                addr_i,
                conf_i['interval'],
                conf_i['payload_size'],
                conf_i['simulation_time'],
                conf_i['rssi'],
                **kwargs
            ),
        )

    return devices


def simulate(devices, capture_effect=False):
    """Count the collisions within the frames of different devices"""

    tmp = devices.copy()
    while len(tmp) > 1:
        curr_dev = tmp.pop()
        for curr_frame in curr_dev:
            check_collisions(curr_frame, tmp, capture_effect)

    collisions_count = 0
    frames_count = 0
    for device in devices:
        frames_count += len(device)
        for frame in device:
            if frame.collision:
                collisions_count += 1

    percentage_lost = collisions_count / frames_count * 100

    return percentage_lost


def simulate_devices(devices, capture_effect=False):
    """Count the collisions within the frames of different devices"""

    tmp = devices.copy()
    while len(tmp) > 1:
        curr_dev = tmp.pop()
        for curr_frame in curr_dev:
            check_collisions(curr_frame, tmp, capture_effect)

    stats = {}
    for device in devices:
        # TODO: this dictionary somehow redo/recopies what is in the Frame.
        # Perhaps is better to have a class "device".
        addr = device[0].address
        interval = device[0].interval
        payload_size = device[0].payload_size
        stats[addr] = {'tx': len(device),
                       'rx': 0,
                       'interval': interval,
                       'payload_size': payload_size}
        for frame in device:
            if not frame.collision:
                stats[addr]['rx'] += 1

    return stats


def run_trials(conf, num_trials=30, **kwargs):
    """Run a given number of simulation trials for the given configuration"""

    trials = []
    for _ in range(num_trials):
        devices = _gen_all_devices(conf, **kwargs)
        trials.append(simulate(devices))

    return trials


def gen_unique_random_address(addresses):
    # TODO: limit the number of tries
    address = gen_random_address()
    while address in addresses:
        address = gen_random_address()

    return address


def prepare_devices_with_same_conf(num_devices, adv_params):
    # nterval, payload_size,
    # simulation_time):
    conf = {}
    for _ in range(num_devices):
        address = gen_unique_random_address(conf)
        conf[address] = adv_params
        #     'interval': interval,
        # 'payload_size': payload_size,
        # 'simulation_time': simulation_time
        # }
    return conf


def load_config_file(config_file):
    config = configparser.ConfigParser()
    config.read(config_file)

    result = {'main': {}, 'devices': {}}
    result['main']['num_trials'] = config.getint('main', 'num_trials',
                                                 fallback=30)
    result['main']['simulation_time'] = config.getint('main',
                                                      'simulation_time',
                                                      fallback=60000000)
    result['main']['seed'] = config.getint('main', 'seed', fallback=None)
    result['main']['description'] = config.get('main', 'description',
                                               fallback=None)

    for section in config.sections():
        # Section 'main' is the main configuration for the simulator. Other
        # sections are considered device configurations
        if section == 'main':
            continue

        address = section
        result['devices'][address] = {}
        interval = config.getint(section, 'interval')
        result['devices'][address]['interval'] = interval
        num_devices = config.getint(section, 'num_devices', fallback=1)
        result['devices'][address]['num_devices'] = num_devices
        payload_size = config.getint(section, 'payload_size')
        result['devices'][address]['payload_size'] = payload_size

    return result


def generate_advertisements(conf, **kwargs):
    """
    Generate the advertisements for all devices described in the configuration

    :conf: dictionary containing the configuration for each device
    :returns: an array of array containing the advertisement frames for all
    devices
    """

    advertisements = []
    for addr_i, conf_i in conf.items():
        advertisements.append(
            _gen_one_device(
                addr_i,
                conf_i['interval'],
                conf_i['payload_size'],
                conf_i['simulation_time'],
                conf_i['rssi'],
                **kwargs
            ),
        )

    return advertisements

# vim:set et sw=4 ts=8 tw=78:
