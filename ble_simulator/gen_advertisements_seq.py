#!/usr/bin python3

"""
Generate the time stamps for the simulation scenarios.

2021-05-06
Laudin Alessandro Molina Troconis <laudin.molina@imt-atlantique.fr>
"""

import os
import os.path
import logging
import random
import argparse

from base import prepare_devices_with_same_conf
from base import generate_advertisements


SCRIPT_NAME = os.path.splitext(os.path.basename(__file__))[0]

logging.basicConfig()
log = logging.getLogger(SCRIPT_NAME)  # pylint: disable=invalid-name
log.setLevel(logging.DEBUG)


def main():  # pylint: disable=missing-function-docstring,too-many-locals
    parser = argparse.ArgumentParser()
    parser.add_argument("--num_trials", default=30, type=int,
                        help="Number of trials to run (30 by default)")
    parser.add_argument("--payload", default=31, type=int,
                        help="Payload size in bytes (31 by default)")
    parser.add_argument("--devices", default=6, type=int,
                        help="Number of devices to simulate (6 by default)")
    parser.add_argument("--time", default=10000000, type=int,
                        help="Simulation time in ms (10000000 us by default)")
    parser.add_argument("--seed", default=-1, type=int,
                        help="Seed for the random generator")
    parser.add_argument("--interval", default=100000, type=int,
                        help="Interval to simulate (100000 us by default)")
    parser.add_argument("--name", help="A name for the simulation")
    parser.add_argument("--header", default=True, action='store_true',
                        help="Show a column header or not")
    parser.add_argument("--no-header", dest='header', action='store_false')
    parser.add_argument("--header-only", default=False, action='store_true',
                        dest='header_only', help="Show the header and exit")

    args = parser.parse_args()

    if args.seed >= 0:
        random.seed(args.seed)
    else:
        random.seed(0)

    num_trials = args.num_trials
    payload_size = args.payload
    num_devices = args.devices
    simulation_time = args.time
    interval = args.interval
    name = args.name

    adv_params = {
        'interval': interval,
        'payload_size': payload_size,
        'simulation_time': simulation_time,
    }
    devs_conf = prepare_devices_with_same_conf(num_devices, adv_params)

    fmt = '{},{},{},{},{},{},{},{}'
    header = fmt.format('trial', 'advInterval', 'devices', 'payload', 'addr',
                        'start', 'end', 'name')
    if args.header_only:
        print(header)
        return

    if args.header:
        print(header)

    for trial in range(num_trials):
        # One trial

        # Generate advertisements for all devices
        advertisements = generate_advertisements(devs_conf)

        for device in advertisements:
            for advertisement in device:
                print(fmt.format(trial, interval, num_devices, payload_size,
                                 advertisement.address,
                                 advertisement.start, advertisement.end,
                                 name))


if __name__ == "__main__":
    main()

# vim:set et sw=4 ts=8 tw=78:
