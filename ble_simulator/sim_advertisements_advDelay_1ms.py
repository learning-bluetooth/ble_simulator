#!/usr/bin python3

"""
Simulate BLE advertisements to evaluate the capacity, i.e., the number of
collisions. Different from sim_advertisements.py this version prints the PDR
per device, while in sim_advertiserments.py the simulator prints the rxError
of the full trial.

2020-09-01
Laudin Alessandro Molina Troconis <laudin.molina@imt-atlantique.fr>
"""

import logging
import random
import argparse

from base import prepare_devices_with_same_conf
from base import simulate_devices
from base import generate_advertisements


def my_adv_delay():
    """Generate advDelay from an uniform [1, 10] with steps of 1 ms

    advDelay in micro seconds
    """

    return random.randrange(0, 10 + 1) * 1000


def main():  # pylint: disable=missing-function-docstring
    # SCRIPT_NAME = os.path.splitext(os.path.basename(__file__))[0]

    parser = argparse.ArgumentParser()
    parser.add_argument("--num_trials", default=30, type=int,
                        help="Number of trials to run (30 by default)")
    parser.add_argument("--payload", default=31, type=int,
                        help="Payload size in bytes (31 by default)")
    parser.add_argument("--devices", default=6, type=int,
                        help="Number of devices to simulate (6 by default)")
    parser.add_argument("--time", default=10000000, type=int,
                        help="Simulation time in ms (10000000 us by default)")
    parser.add_argument("--seed", default=-1, type=int,
                        help="Seed for the random generator")
    parser.add_argument("--interval", default=100000, type=int,
                        help="Interval to simulate (100000 us by default)")
    parser.add_argument("--name", help="A name for the simulation")
    parser.add_argument("--header", default=True, action='store_true',
                        help="Show a column header or not")
    parser.add_argument("--no-header", dest='header', action='store_false')
    parser.add_argument("--stats-footer", default=False,
                        dest='stats_footer', action='store_true',
                        help="Show a sum up line at the end with the "
                        "average error rate over all trials")
    parser.add_argument("--capture-effect", default=False,
                        dest='capture_effect', action='store_true',
                        help="When present, the simulator will assume that "
                        "one of the frames captures the channel and is "
                        "correctly decoded.")
    parser.add_argument("--no-capture-effect", action='store_false',
                        dest='capture_effect')
    parser.add_argument("--header-only", default=False, action='store_true',
                        dest='header_only', help="Show the header and exit")
    parser.add_argument("--loglevel", default='ERROR')

    args = parser.parse_args()

    numeric_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % args.loglevel)
    logging.basicConfig(level=numeric_level)

    if args.seed >= 0:
        random.seed(args.seed)
    else:
        random.seed(0)

    num_trials = args.num_trials
    payload_size = args.payload
    num_devices = args.devices
    simulation_time = args.time
    interval = args.interval
    name = args.name

    adv_params = {
        'interval': interval,
        'payload_size': payload_size,
        'simulation_time': simulation_time,
    }
    devs_conf = prepare_devices_with_same_conf(num_devices, adv_params)

    header = 'trial,addr,advInterval,devices,payload,pdr,tx,rx,name'
    if args.header_only:
        print(header)
        return

    if args.header:
        print(header)

    for trial in range(num_trials):
        # One trial

        # Generate advertisements for all devices
        advertisements = generate_advertisements(devs_conf,
                                                 adv_delay=my_adv_delay)

        # Count frames that arrive to the destination (i.e., those that didn't
        # collide)
        stats = simulate_devices(advertisements, args.capture_effect)
        for addr, device in stats.items():
            tx = device['tx']
            rx = device['rx']
            pdr = rx / tx
            print(f'{trial},{addr},{interval},{num_devices},{payload_size},{pdr},{tx},{rx},{name}')


if __name__ == "__main__":
    main()

# vim:set et sw=4 ts=8 tw=78:
