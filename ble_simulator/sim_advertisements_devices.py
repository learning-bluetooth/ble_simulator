#!/usr/bin python3

"""
Simulate BLE advertisements to evaluate the capacity, i.e., the number of
collisions. Different from sim_advertisements.py this version prints the PDR
per device, while in sim_advertiserments.py the simulator prints the rxError
of the full trial.

2020-09-01
Laudin Alessandro Molina Troconis <laudin.molina@imt-atlantique.fr>
"""

import logging
import random
import argparse
import configparser

from base import prepare_devices_with_same_conf
from base import simulate_devices
from base import generate_advertisements


# TODO: move to a separated file? Perhaps utils?
def read_config_file(config_file):
    """
    Read and pre-process a configuration file

    :config_file: path to the configuration file (compatible with
    configparser)

    :returns: a dictionary with
    """
    config = configparser.ConfigParser()
    config.read(config_file)

    result = {'main': {}, 'devices': {}}
    result['main']['num_trials'] = config.getint('main', 'num_trials',
                                                 fallback=30)
    result['main']['simulation_time'] = config.getint('main',
                                                      'simulation_time',
                                                      fallback=60000000)
    result['main']['seed'] = config.getint('main', 'seed', fallback=None)
    result['main']['name'] = config.get('main', 'name', fallback=None)
    result['main']['capture_effect'] = config.getboolean('main',
                                                         'capture_effect',
                                                         fallback=True)

    for section in config.sections():
        # Section 'main' is the main configuration for the simulator. Other
        # sections are considered device configurations
        if section == 'main':
            continue

        address = section
        result['devices'][address] = {}
        interval = int(config.getfloat(section, 'interval'))
        result['devices'][address]['interval'] = interval

        num_devices = config.getint(section, 'num_devices', fallback=1)
        result['devices'][address]['num_devices'] = num_devices

        payload_size = config.getint(section, 'payload_size')
        result['devices'][address]['payload_size'] = payload_size

        rssi = config.getfloat(section, 'rssi')
        result['devices'][address]['rssi'] = rssi

    return result


def main():  # pylint: disable=missing-function-docstring
    # SCRIPT_NAME = os.path.splitext(os.path.basename(__file__))[0]

    parser = argparse.ArgumentParser()
    parser.add_argument("--config", default=None,
                        help="Path to the configuration of the simulation")
    parser.add_argument("--num_trials", default=30, type=int,
                        help="Number of trials to run (30 by default)")
    parser.add_argument("--payload", default=31, type=int,
                        help="Payload size in bytes (31 by default)")
    parser.add_argument("--devices", default=6, type=int,
                        help="Number of devices to simulate (6 by default)")
    parser.add_argument("--time", default=10000000, type=int,
                        help="Simulation time in ms (10000000 us by default)")
    parser.add_argument("--seed", default=-1, type=int,
                        help="Seed for the random generator")
    parser.add_argument("--interval", default=100000, type=int,
                        help="Interval to simulate (100000 us by default)")
    parser.add_argument("--name", help="A name for the simulation")
    parser.add_argument("--header", default=True, action='store_true',
                        help="Show a column header or not")
    parser.add_argument("--no-header", dest='header', action='store_false')
    parser.add_argument("--stats-footer", default=False,
                        dest='stats_footer', action='store_true',
                        help="Show a sum up line at the end with the "
                        "average error rate over all trials")
    parser.add_argument("--capture-effect", default=False,
                        dest='capture_effect', action='store_true',
                        help="When present, the simulator will assume that "
                        "one of the frames captures the channel and is "
                        "correctly decoded.")
    parser.add_argument("--no-capture-effect", action='store_false',
                        dest='capture_effect')
    parser.add_argument("--header-only", default=False, action='store_true',
                        dest='header_only', help="Show the header and exit")
    parser.add_argument("--loglevel", default='ERROR')

    args = parser.parse_args()

    numeric_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % args.loglevel)
    logging.basicConfig(level=numeric_level)

    header = 'trial,addr,advInterval,devices,payload,pdr,tx,rx,name'
    if args.header_only:
        print(header)
        return

    if args.header:
        print(header)

    # Read configuration from file and ignore other parameters
    # TODO: a better approach may be to read configuration file and
    # override values using the command line parameters
    if args.config is not None:
        config = read_config_file(args.config)

        name = config['main']['name']
        seed = config['main']['seed']
        simulation_time = config['main']['simulation_time']
        num_trials = config['main']['num_trials']
        capture_effect = config['main']['capture_effect']

    else:
        seed = args.seed
        simulation_time = args.time
        num_trials = args.num_trials
        capture_effect = args.capture_effect

        payload_size = args.payload
        num_devices = args.devices
        interval = args.interval
        name = args.name

        # TODO: make something useful with the rssi
        rssi = 0

        adv_params = {
            'num_devices': num_devices,
            'interval': interval,
            'payload_size': payload_size,
            'rssi': rssi,
        }
        config = {'devices': {'dev0': adv_params}}

    if seed is not None:
        random.seed(seed)
    else:
        random.seed(0)

    num_devices = 0
    devs_conf = {}
    for device in config['devices'].values():
        num_devices += device['num_devices']
        adv_params = device.copy()
        del adv_params['num_devices']
        adv_params['simulation_time'] = simulation_time
        tmp = prepare_devices_with_same_conf(device['num_devices'],
                                             adv_params)
        devs_conf.update(tmp)


    for trial in range(num_trials):
        # One trial

        # Generate advertisements for all devices
        advertisements = generate_advertisements(devs_conf)

        # Count frames that arrive to the destination (i.e., those that didn't
        # collide)
        stats = simulate_devices(advertisements, capture_effect)
        for addr, device in stats.items():
            interval = device['interval']
            payload_size = device['payload_size']
            tx = device['tx']
            rx = device['rx']
            pdr = rx / tx
            print(f'{trial},{addr},{interval},{num_devices},{payload_size},'
                  f'{pdr},{tx},{rx},{name}')


if __name__ == "__main__":
    main()

# vim:set et sw=4 ts=8 tw=78:
