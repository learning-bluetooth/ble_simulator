#!/usr/bin/env bash
#
# advDelay = 100ms
# payload = 10, 15, 20, 25
# devices = 1 to 24

tmp="$(basename $0)"
outfile="${tmp%.*}.csv"

SEED=0
INTERVALS=(100)
PAYLOAD=(10 15 20 25 31)
DEVICES=($(seq 1 24))
TIME=200000 # 10/3 min

echo "${0} git rev $(git rev-parse HEAD) ; $(date) ; $(hostname -f)" | \
  tee ${outfile}

python3 sim_advertisements_devices.py \
  --payload ${PAYLOAD[0]} \
  --devices ${DEVICES[0]} \
  --seed ${SEED} \
  --interval ${INTERVALS[0]} \
  --time ${TIME} | \
    tee -a ${outfile}

for devices in "${DEVICES[@]:1}";
do
  python3 sim_advertisements_devices.py \
    --payload ${PAYLOAD[0]} \
    --devices ${devices} \
    --seed ${SEED} \
    --interval ${INTERVALS[0]} \
    --time ${TIME} \
    --no-header | \
    tee -a ${outfile}
done

for payload in "${PAYLOAD[@]:1}";
do
  printf '%s\n' "${DEVICES[@]}" | parallel -k \
             python3 sim_advertisements_devices.py \
             --payload ${payload} \
             --devices {} \
             --seed ${SEED} \
             --interval ${INTERVALS[0]} \
             --time ${TIME} \
             --no-header | \
      tee -a ${outfile}
done
