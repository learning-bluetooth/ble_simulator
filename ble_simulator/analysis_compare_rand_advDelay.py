#!/usr/bin/env python
# coding: utf-8

# In[2]:


import os.path

import matplotlib.pyplot as plt
import pandas as pd

import seaborn as sns
sns.set(style="whitegrid")


def rotate(x):
    plt.setp(x.get_xticklabels(), rotation=45, ha='right')
    return x


# In[4]:


sim_base = '~/src/apero/simulations/'
traces = 'sim_02.csv'

# Row 0 includes some metadata for future reference and tracking purposes
df = pd.read_csv(os.path.join(sim_base, traces), skiprows=[0],
                 header=None,
                 names=('advInterval', 'devices', 'payload', 'rxError',
                        'advDelay'))

df.head()

df20 = df.query('advInterval == 20')


# In[7]:


df20.groupby(['devices', 'advDelay'])['rxError'].mean().unstack().plot()


# In[9]:


data = df.query('advInterval in ("20", "50", "100", "500", "1000")')
data['advInterval'] = pd.Categorical(data.advInterval)

fig, ax = plt.subplots()
sns.lineplot(
    data=data,
    x="devices", y="rxError",
    style="advDelay",
    hue="advInterval",
    ci=95,
    marker=True,
    ax=ax)

# Save files with the format SCRIPT_NAME_TRACE.png
SCRIPT_NAME = os.path.splitext(os.path.basename(__file__))[0]
filename = '_'.join([SCRIPT_NAME, os.path.splitext(traces)[0]])
plt.savefig(f'{filename}.png', filetype='png')

ax.set_xlim([1, 10])
ax.set_ylim([0, 35])
plt.savefig(f'{filename}_zoom.png', filetype='png')
