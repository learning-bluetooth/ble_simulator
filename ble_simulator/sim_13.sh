#!/usr/bin/env bash
#
# Simulate advertisements to compare with empirical results, compare with
# experimental
# <PARTAGE>/Chantier 3.0/traces/faraday/38/payload31/100ms/
#
# advDelay = 100ms
# payload = 31
# devices = 1 to 15

tmp="$(basename $0)"
outfile="${tmp%.*}.csv"

SEED=0
INTERVALS=(100)
PAYLOAD=31
DEVICES=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 $(seq 20 10 100) $(seq 150 10 250) $(seq 300 100 1000))
TIME=200000 # 10/3 min
TRIALS=30

echo "${0} git rev $(git rev-parse HEAD) ; $(date) ; $(hostname -f)" | \
  tee ${outfile}

python3 sim_advertisements_devices.py \
  --num_trials ${TRIALS} \
  --payload ${PAYLOAD} \
  --devices ${DEVICES[0]} \
  --seed ${SEED} \
  --interval ${INTERVALS[0]} \
  --time ${TIME} | \
    tee -a ${outfile}

printf '%s\n' "${DEVICES[@]}" | parallel -k \
					 python3 sim_advertisements_devices.py \
           --num_trials ${TRIALS} \
					 --payload ${PAYLOAD} \
					 --devices {} \
					 --seed ${SEED} \
					 --interval ${INTERVALS[0]} \
					 --time ${TIME} \
					 --no-header | \
    tee -a ${outfile}
